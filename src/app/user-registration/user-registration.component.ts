import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {FormControl} from '@angular/forms';
import { forbiddenNameValidator } from '../username-constraint-validation';
import { PasswordValidator } from '../password-validator';
@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  constructor(private _fb:FormBuilder) { }

  registrationForm = this._fb.group(
    {
      username : ['Hussein', [Validators.required,Validators.minLength(3),forbiddenNameValidator]],
      password : [''],
      confirmPassword : [''],
      address : this._fb.group({
        city : ['Bisha'],
        state : ['Asir'],
        postalCode : ['1234']
      })
    },{validator : PasswordValidator});
  ngOnInit() {
  }

  loadData()
  {
    this.registrationForm.patchValue({
      username : 'Hussein',
      password :'test123',
      confirmPassword : 'test123'
    });
  }

}
