import {AbstractControl } from "@angular/forms";

export function forbiddenNameValidator(control:AbstractControl)
{
    if(control.value == 'Hussein')
    {
        return {'forbiddenName' : control.value};
    }else
    {
        return null;
    }
}